import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoginService } from 'src/app/services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  credentials = {
    username: "",
    password: ""
  }
  constructor(private snackBar: MatSnackBar, private loginService: LoginService) { }

  ngOnInit(): void {
  }

  userDataSubmit() {
    if ((this.credentials.username != '' && this.credentials.password != '') &&
      (this.credentials.username != null && this.credentials.password != null)) {

      this.loginService.generateToken(this.credentials).subscribe(
        response => {
          console.warn(response);
          this.snackBar.open('Successfully Login', 'Close', { duration: 3000 });
        },
        error => {
          console.warn(error);
          this.snackBar.open('Bad Credentials !  !  !  !', 'Close', { duration: 3000 });
        }
      )

    } else {
      this.snackBar.open('Fill all the Fields Properly ! ! ! !', 'Close', { duration: 3000 });
    }
  }


}
