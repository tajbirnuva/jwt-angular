import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SignupService } from 'src/app/services/signup/signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public data = {
    firstName: "",
    lastName: "",
    username: "",
    password: "",
    email: "",
    mobile: "",
  }

  constructor(private snackBar: MatSnackBar, private signupService: SignupService) { }

  ngOnInit(): void {
  }

  userDataSubmit() {
    if ((this.data.username != '' && this.data.password != '' && this.data.firstName != ''
      && this.data.lastName != '' && this.data.email != '' && this.data.mobile != '') &&
      (this.data.username != null && this.data.password != null && this.data.firstName != null
        && this.data.lastName != null && this.data.email != null && this.data.mobile != null)) {
      this.signupService.signUp(this.data).subscribe(
        (response: any) => {
          if (response.message == "Username Already Used") {
            this.snackBar.open("Username Already Used", "Close", { duration: 3000 });
          } else {
            this.snackBar.open("Successful ! ! !", "Close", { duration: 3000 });
          }
        },
        error => {
          console.log(error);
          this.snackBar.open("Fail Operation !  !  !  !", "Close", { duration: 3000 });
        }
      )
    } else {
      this.snackBar.open('Fill all the Fields Properly ! ! ! !', 'Close', { duration: 3000 });
    }
  }
}
