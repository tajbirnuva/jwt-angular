import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseUrl: string = "http://localhost:8080";

  constructor(private http: HttpClient) { }

  generateToken(credentials: any) {
    return this.http.post(`${this.baseUrl}/token`, credentials);
  }

  loginUser(token) {
    localStorage.setItem("token", token);
    return true;
  }

  isLoggedIn() {
    let token = localStorage.getItem("token");

    if (token == undefined || token === '' || token == null) {
      return false
    } else {
      return true
    }
  }

  getToken() {
    return localStorage.getItem("token");
  }


  logout() {
    localStorage.removeItem("token");
    return true;
  }

}
