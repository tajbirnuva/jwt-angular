import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SignupService {
  baseUrl: string = "http://localhost:8080";

  constructor(private http: HttpClient) { }

  signUp(data: any) {
    return this.http.post(`${this.baseUrl}/signup/`, data);
  }

}
